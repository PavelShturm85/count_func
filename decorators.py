import random
import time 

# class calls():
#   counter = {}
#
#   @classmethod
#   def count(cls, proto):
#     def post(*args, **kwargs):
#       cls.counter[proto.__name__] = cls.counter.get(proto.__name__, 0) + 1
#       return proto(*args, **kwargs)
#
#     return post
#
# @calls.count
# def abc():
#   return 'abc'
#
# @calls.count
# def hi():
#   return 'hi'
#
#
# if __name__ == '__main__':
#   funcs = (abc, hi)
#   for _ in xrange(10):
#     print random.choice(funcs)()
#   print calls.counter
#


class count_time_to_do():
    def __init__(self, function):
      self.action = function
      self.delta = ''

    def __call__(self, *args, **kwargs):
      time_before = time.time()
      action = self.action(*args, **kwargs)
      time_after = time.time()
      self.delta = time_after - time_before
      return action

    def __str__(self):
      return '{} do times {}'.format(str(self.action), self.delta)


def count_time_to_do_func(func):
    def wrap(*args, **kwargs):
        time_before = time.time()
        act = func(*args, **kwargs)
        time_after = time.time()
        delta = time_after - time_before
        print("func {} time delay {}".format(func.__name__, delta))
        return act
    return wrap


class call_counting_function():
    def __init__(self, function):
      self.counter = 0
      self.action = function

    def __call__(self, *args, **kwargs):
      self.counter += 1
      return self.action(*args, **kwargs)

    def __str__(self):
      return '{} being called {} times'.format(str(self.action), self.counter)


@count_time_to_do
@call_counting_function
def foo():
    sec = random.randint(1, 4)
    time.sleep(sec)
    return 'foo'


@count_time_to_do
@call_counting_function
def bar():
    sec = random.randint(1, 2)
    time.sleep(sec)
    return 'bar'

if __name__ == '__main__':
    funcs = (foo, bar)
    for _ in range(10):
      print (random.choice(funcs)())
    print (foo)
    print (bar)
